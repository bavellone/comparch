library ieee;
use ieee.std_logic_1164.all;
use work.common.all;
use ieee.Numeric_Std.all;

entity ramModule is
	generic(
		addrWidth: integer := 4;
		dataWidth: integer := 8;
		initialData: std_logic_vector;
		initialize: std_logic := '1'
	);
	port (
	    clk, we: in std_logic;
	    addr: in std_logic_vector(addrWidth-1 downto 0);
	    dataIn: in std_logic_vector(dataWidth-1 downto 0);
	    dataOut: out std_logic_vector(dataWidth-1 downto 0)
  );
end entity ramModule;

architecture RTL of ramModule is

   type ram_type is array (0 to addrWidth-1) of std_logic_vector(dataWidth-1 downto 0);
   signal ram : ram_type := (others => (others => '0'));
   signal readAddr : std_logic_vector(addrWidth-1 downto 0);

begin
	
  process(clk) is
  begin
	
    if rising_edge(clk) then
	 
	  if (initialize = '1') then
		ram(0) <= initialData;
		ram(1) <= initialData;
		ram(2) <= initialData;
		ram(3) <= initialData;
	end if;
	 
      if we = '1' then
        ram(to_integer(unsigned(addr))) <= datain;
      end if;
      readAddr <= addr;
    end if;
  end process;
  dataout <= ram(to_integer(unsigned(readAddr)));
  
end architecture RTL;