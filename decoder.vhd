library ieee;
use ieee.std_logic_1164.all;
use work.common.all;

ENTITY decoder IS 
	generic (
		selWidth: integer := 3
	);
	port (
		En : in std_logic;
		dataIn : in std_logic_vector(selWidth-1 downto 0) ;
		dataOut  : out std_logic_vector((selWidth**2)-1 downto 0)
	);
END decoder;

ARCHITECTURE Behavior OF decoder IS
BEGIN
	process(En, dataIn)
	begin
		if (En = '1') then
			case dataIn is
				when "00" => dataOut <= "0001";
				when "01" => dataOut <= "0010";
				when "10" => dataOut <= "0100";
				when "11" => dataOut <= "1000";
				when others => dataOut <= "0000";
			end case;
		else
			dataOut <= "0000";
		end if;
	end process;
END Behavior;