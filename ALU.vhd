library ieee;
use ieee.std_logic_1164.all;
use work.common.all;
use ieee.std_logic_signed.all;

entity ALU is
	port(
		ALUop: in std_logic_vector(1 downto 0);
		dataIn1: in std_logic_vector;
		dataIn2: in std_logic_vector;
		dataOut: out std_logic_vector
	);
end entity ALU;

architecture ALU_Ctrl of ALU is
begin 
	dataOut <=  dataIn1 + dataIn2 when ALUop = "00" else
			dataIn1 - dataIn2 when ALUop = "01" else
			dataIn1 and dataIn2 when ALUop = "10" else
			dataIn1 or dataIn2 when ALUop = "11";

end architecture ALU_Ctrl;