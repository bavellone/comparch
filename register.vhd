library ieee;
use ieee.std_logic_1164.all;
use work.common.all;

entity dataRegister is
	generic(
		regWidth: integer := 8
	);
	port (
		clk, load : in std_logic;
		dataIn : in std_logic_vector(regWidth-1 downto 0);
		dataOut : buffer std_logic_vector(regWidth-1 downto 0)
	);
end entity dataRegister;

architecture RTL of dataRegister is
begin
	process (clk, dataOut, load)
	begin
		if (falling_edge(clk) and load = '1') then
			dataOut <= dataIn;
		else
			dataOut <= dataOut;
		end if;
		end process;
end architecture RTL;
