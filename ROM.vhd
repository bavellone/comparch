library ieee;
use ieee.std_logic_1164.all;
use work.common.all;
use ieee.Numeric_Std.all;

entity ROM is
	port (
	    addr: in std_logic_vector;
	    dataIn: in ROMdata;
	    dataOut: out std_logic_vector
  );
end entity ROM;

architecture RTL of ROM is

begin
	
  dataout <= dataIn(to_integer(unsigned(addr)));
  
end architecture RTL;