library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

package common is
	-- Data memory
	constant DataMemData : integer := 8;
	constant DataMemAddr : integer := 4;
	
	-- Instruction memory
	constant InstrMemData : integer := 12;
	constant InstrMemAddr : integer := 6;
	
	-- Registers
	constant NumReg: integer := 4;
	constant RegSel : integer := 2;
	
	constant SelWidth: integer := 2;
	constant OpcodeLength: integer := 3;
	constant OperandLength: integer := 2;
	
	type ROMdata is array (0 to InstrMemAddr**2-1) of std_logic_vector(InstrMemData-1 downto 0);
end package common;

package body common is
	
end package body common;
