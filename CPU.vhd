library ieee;
use ieee.std_logic_1164.all;
use work.common.all;

entity CPU is
	port (
		clk : in std_logic;
		writeReg : buffer std_logic;
		instruction: buffer std_logic_vector(InstrMemData-1 downto 0) := "011100000000";
		destRegSel, srcReg1Sel, srcReg2Sel, RegInSel: buffer std_logic_vector(RegSel-1 downto 0);
		srcReg1, srcReg2, RegIn: buffer std_logic_vector(DataMemData-1 downto 0);
		PCIn, PCOut : buffer std_logic_vector(InstrMemAddr-1 downto 0);
		memDataIn, memDataOut, immData: buffer std_logic_vector(DataMemData-1 downto 0);
		RegInData: buffer std_logic_vector(DataMemData*NumReg-1 downto 0)
	);
end entity CPU;

architecture RTL of CPU is
	-------------------------------
	-------- Control paths --------
	-------------------------------
	
	-------- CU ---------
	signal branch: std_logic;
	signal Offset: std_logic_vector(5 downto 0);
	--signal instruction: std_logic_vector(InstrMemData-1 downto 0);
	
	-------- ALU --------
	-- Controls the operation of the ALU
	signal ALUop : std_logic_vector(1 downto 0);
	signal comparator: std_logic;
	
	-------- Memory --------
	-- Controls which memory location is being accessed
	signal memAddr: std_logic_vector(DataMemAddr-1 downto 0);
	-- Controls whether we are reading or writing to memory
	signal memReadWrite: std_logic;
	
	------- Registers ------
	-- Controls whether register will be written to
	--signal writeReg : std_logic;
	-- These signals control which registers are active
	--signal destRegSel, srcReg1Sel, srcReg2Sel, RegInSel: std_logic_vector(RegSel-1 downto 0);
	
	
	----------------------------
	-------- Data paths --------
	----------------------------
	
	-------- Memory --------
	-- Memory data and data from instructions
	--signal memDataIn, memDataOut, immData: std_logic_vector(DataMemData-1 downto 0); 
	
	-------- Busses --------
	--signal srcReg1, srcReg2, RegIn : std_logic_vector(DataMemData-1 downto 0);
	--signal RegInData: std_logic_vector(DataMemData*NumReg-1 downto 0);
	
	--------- ALU ----------
	signal ALUOut : std_logic_vector(DataMemData-1 downto 0);
	
	------- Registers ------
	-- The outputs from the register file
	signal RegOut1: std_logic_vector(DataMemData-1 downto 0);
	signal RegOut2: std_logic_vector(DataMemData-1 downto 0);
	
	--------- PC ----------- 
	--signal PCIn, PCOut : std_logic_vector(InstrMemAddr-1 downto 0);
	signal PCextended, PCnext, BranchAddr : std_logic_vector(InstrMemAddr downto 0);
	signal PCMuxsignal : std_logic_vector(InstrMemAddr*2+1 downto 0);
	signal PCsel: std_logic;
	
	signal instructionArray: ROMdata := (0 => "000000000001", 1=> "100010100101", others => (others => '0'));
	
begin
	RegInData <= memDataOut & immData & srcReg1 & ALUOut;
	immData <= instruction(7 downto 0);
	memDataIn <= RegOut1;
	PCsel <= branch and comparator;
	PCextended <= '0' & PCOut;
	PCMuxsignal <= BranchAddr & PCnext;
	Offset <= instruction(5 downto 0);
	
	ControlUnit: entity work.controlUnit
		port map(clk          => clk,
			     instruction  => instruction,
			     memAddr      => memAddr,
			     memReadWrite => memReadWrite,
			     srcReg1Sel    => srcReg1Sel,
				 srcReg2Sel    => srcReg2Sel,
			     destRegSel   => destRegSel,
			     destInputSel   => RegInSel,
			     ALUop       => ALUop,
			     branch => branch,
				 writeReg   => writeReg);
	
	ProgramCounter: entity work.dataRegister
		generic map(
			regWidth => InstrMemAddr
		)
		port map(clk     => clk,
			     load    => '1',
			     dataIn  => PCIn,
			     dataOut => PCOut);
	
	DataMem: entity work.ramModule
		generic map(
			addrWidth => DataMemAddr,
			dataWidth => DataMemData,
			initialize => '0',
			initialData => "00000000"
			)
		port map(clk     => clk,
			     we      => memReadWrite,
			     addr    => memAddr,
			     dataIn  => memDataIn,
			     dataOut => memDataOut);
	
	InstrMem: entity work.ROM
		port map(addr    => PCOut,
			     dataIn  => instructionArray,
			     dataOut => instruction);
	
	RegInBus: entity work.busMux
		port map(sel     => RegInSel,
			     dataIn  => RegInData,
			     dataOut => RegIn);
	
	PCMux: entity work.busMux
		port map(sel     => (0 => PCsel),
			     dataIn  => PCMuxsignal,
			     dataOut => PCIn);
	
	RegFile: entity work.RegFile
	generic map(
		numReg => NumReg,
		regWidth => DataMemData
	)
	port map(
		clk       => clk,
		writeEn   => writeReg,
		src1      => srcReg1Sel,
		src2      => srcReg2Sel,
		writeDest => destRegSel,
		dataOut1  => RegOut1,
		dataOut2  => RegOut2,
		regDataIn    => RegIn
	);
	
	DataALU: entity work.ALU
		port map(ALUop  => ALUop,
			     dataIn1 => srcReg1,
			     dataIn2 => srcReg2,
			     dataOut => ALUOut);
	
	PCALU: entity work.ALU
		port map(ALUop  => "00",
			     dataIn1 => PCextended,
			     dataIn2 => "0000001",
			     dataOut => PCNext);
	
	BranchALU: entity work.ALU
		port map(ALUop  => "00",
			     dataIn1 => PCNext,
			     dataIn2 => Offset,
			     dataOut => BranchAddr);
	
	RegEqual: entity work.Comparator
		port map(
			dataIn1 => RegOut1,
			dataIn2 => RegOut2,
			dataOut => comparator
		);
	
end architecture RTL;
