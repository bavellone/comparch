library ieee;
use ieee.std_logic_1164.all;
use work.common.all;

entity RegFile is
	generic (
		numReg: integer := 4;
		regWidth: integer := 8
	);
	port (
		clk, writeEn : in std_logic;
		src1, src2, writeDest : std_logic_vector(RegSel-1 downto 0);
		dataOut1, dataOut2 : out std_logic_vector(DataMemData-1 downto 0);
		regDataIn : in std_logic_vector(DataMemData-1 downto 0)
	);
end entity RegFile;

architecture Behavior of RegFile is
	-- Controls which register will be written to
	signal destRegEn: std_logic_vector(numReg-1 downto 0);
	signal regOut: std_logic_vector(numReg*regWidth-1 downto 0);
begin
	
	RegSelDecoder: entity work.decoder
		generic map(
			selWidth => RegSel
		)
		port map(En      => '1',
			     dataIn  => writeDest,
			     dataOut => destRegEn);
	
	SrcRegBus1: entity work.busMux
		port map(sel     => src1,
			     dataIn  => regOut,
			     dataOut => dataOut1);
	
	SrcRegBus2: entity work.busMux
		port map(sel     => src2,
			     dataIn  => regOut,
			     dataOut => dataOut2);
	
	
	REG_GEN: for i in 0 to numReg-1 generate
		REGX: entity work.dataRegister port map(
			clk, 
			writeEn and destRegEn(i), 
			regDataIn, 
			regOut(regWidth*i+regWidth-1 downto regWidth*i)
		);
	end generate;

end architecture Behavior;
