library ieee;
use ieee.std_logic_1164.all;
use work.common.all;

entity controlUnit is
	port (
		clk: in std_logic;
		instruction: in std_logic_vector(InstrMemData-1 downto 0) := "011100000000";
		memAddr: out std_logic_vector(DataMemAddr-1 downto 0);
		memReadWrite, writeReg, branch : out std_logic;
		ALUop : out std_logic_vector(1 downto 0);
		srcReg1Sel, srcReg2Sel, destRegSel, destInputSel: out std_logic_vector(SelWidth-1 downto 0) := "00"
	);
end entity controlUnit;

architecture RTL of controlUnit is
	type formatType is (R, I, B);
	
	-- R-format instruction vectors
	signal Rop: std_logic_vector(2 downto 0);
	signal Rsrc1, Rsrc2, Rdest: std_logic_vector(1 downto 0);
	
	-- I-format instruction vector
	signal Idest: std_logic_vector(1 downto 0);
	signal Idata: std_logic_vector(7 downto 0);
	
	-- B-format instruction vector
	signal Bsrc1, Bsrc2: std_logic_vector(1 downto 0);
begin
	-- R-format
	Rop <= instruction(10 downto 8);
	Rsrc1 <= instruction(7 downto 6);
	Rsrc2 <= instruction(5 downto 4);
	Rdest <= instruction(3 downto 2);
	
	-- I-format
	Idest <= instruction(9 downto 8);
	Idata <= instruction(7 downto 0);
	
	-- B-format
	Bsrc1 <= instruction(9 downto 8);
	Bsrc2 <= instruction(7 downto 6);
	
	process(clk)
		variable instructionType: formatType;
	begin
		
		if (rising_edge(clk)) then
		
			-- Determine instruction type
			if (instruction(11) = '0') then
				instructionType := R;
			elsif (instruction(10) = '0') then
				instructionType := I;
			else
				instructionType := B;
			end if;
			
			writeReg <= '0';
			memReadWrite <= '0';
			branch <= '0';
			ALUop <= "00";
			destInputSel <= "00";
		
			if (instructionType = I) then -- I-format
				
				destRegSel <= Idest; -- Set dest reg
				destInputSel <= "10"; -- Use immediate data
				writeReg <= '1';
				
			elsif (instructionType = B) then -- B-format
				
				branch <= '1'; -- Set branch
				srcReg1Sel <= Bsrc1; -- Set src registers
				srcReg2Sel <= Bsrc2;
				
			else -- R-format
				case Rop is
				when "111" => -- Load from Memory: destReg, upper half, lower half
					destRegSel <= Rsrc1;
					memAddr <= Rsrc2 & Rdest;
					destInputSel <= "11";
					writeReg <= '1';
				when "001" => -- Store to Memory: srcReg, upper half, lower half
					srcReg1Sel <= Rsrc1;
					memAddr <= Rsrc2 & Rdest;
					memReadWrite <= '1';
				when "010" => -- Move: src, xx, dest
					srcReg1Sel <= Rsrc1;
					destRegSel <= Rdest;
					destInputSel <= "01";
					writeReg <= '1';
				when "011" => -- Add: src1, src2, dest
					ALUop <= "00";
					srcReg1Sel <= Rsrc1;
					srcReg2Sel <= Rsrc2;
					destRegSel <= Rdest;
					destInputSel <= "11";
					writeReg <= '1';
				when "100" => -- Sub: src1, src2, dest
					ALUop <= "01";
					srcReg1Sel <= Rsrc1;
					srcReg2Sel <= Rsrc2;
					destRegSel <= Rdest;
					destInputSel <= "11";
					writeReg <= '1';
				when "101" => -- And: src1, src2, dest
					ALUop <= "10";
					srcReg1Sel <= Rsrc1;
					srcReg2Sel <= Rsrc2;
					destRegSel <= Rdest;
					destInputSel <= "11";
					writeReg <= '1';
				when "110" => -- Or: src1, src2, dest
					ALUop <= "11";
					srcReg1Sel <= Rsrc1;
					srcReg2Sel <= Rsrc2;
					destRegSel <= Rdest;
					destInputSel <= "11";
					writeReg <= '1';
				when others => null; -- No-op
				end case;
			end if;
			
		end if;

	end process;
	
end architecture RTL;
		
-- 001 10 1111		r2 = F
-- 001 11 1010		r3 = A
-- 011 10 1000		m1000 = r2 (F)
-- 010 01 1000		r1 = m1000 (F)
-- 001 00 1000		r0 = 8
-- 100 00 11		r3 = r0 (8)
-- 001 11 0001		r3 = 1
-- 101 00 11 10	r2 = r0 + r3 (8 + 1 = 9)
-- 110 00 11 01	r1 = r0 - r3 (8 - 1 = 7)

