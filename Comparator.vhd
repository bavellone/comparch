library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_signed.all;

entity Comparator is
	port(
		dataIn1: in std_logic_vector;
		dataIn2: in std_logic_vector;
		dataOut: out std_logic
	);
end entity Comparator;

architecture Comparator_Ctrl of Comparator is
begin
	dataOut <= '1' when dataIn1 = dataIn2 
				else '0';
end architecture Comparator_Ctrl;