library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity BusMux is
	port (
		sel: in std_logic_vector;
		dataIn: in std_logic_vector;
		dataOut: out std_logic_vector
	);
end entity BusMux;

architecture BusArch of BusMux is
begin
	-- This bus does the following:
	-- out = in[ (inputIndex*width) + width-1 downto inputIndex*width]
	-- I.e. for a 4x1 mux with 2 bit data lines, with sel=01:
	-- out = in(1*2+1 downto 1*2)
	-- and with sel = 11:
	-- out = in(3*2+1 downto 3*2)
	dataOut <= dataIn((to_integer(unsigned(sel))*dataOut'length)+dataOut'length-1 downto (to_integer(unsigned(sel)) * dataOut'length));
end architecture BusArch;
